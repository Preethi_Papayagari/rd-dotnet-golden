﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Apartment.Models
{
    public class MonthlyMaintainance
    {
            public int ID { get; set; }
            public int ResidentId { get; set; }
            public string Month { get; set; }
            public int Year { get; set; }
            public Decimal MaintenanceAmount { get; set; }
            public string Note { get; set; }
    }
}