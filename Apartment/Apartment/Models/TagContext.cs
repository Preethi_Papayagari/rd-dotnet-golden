﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Apartment.Models
{
    public class TagContext:DbContext
    {
        public TagContext():base("Code")
        { }
        public DbSet<Tag> Tags { get; set; }

    }
}