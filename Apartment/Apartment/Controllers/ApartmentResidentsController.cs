﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;
using Apartment.Models;

namespace Apartment.Controllers
{
    public class ApartmentResidentsController : Controller
    {
        private Apartment_Expenses2Entities db = new Apartment_Expenses2Entities();

        // GET: ApartmentResidents
        public Boolean IsAuthorize()
        {
            if (!User.Identity.Name.IsEmpty())
            {
                return true;
            }
            return false;
        }
        public ActionResult Index()
        {

            if (IsAuthorize())
            {
                return View(db.ApartmentResidents.ToList());
            }
            return RedirectToAction("LogIn", "LogIn");
        }

        // GET: ApartmentResidents/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApartmentResident apartmentResident = db.ApartmentResidents.Find(id);
            if (apartmentResident == null)
            {
                return HttpNotFound();
            }
            if (IsAuthorize())
            {
                return View(apartmentResident);
            }
            return RedirectToAction("LogIn", "LogIn");
        
    }

        // GET: ApartmentResidents/Create
        public ActionResult Create()
        {
            if (IsAuthorize())
            {
                return View();
            }
            return RedirectToAction("LogIn", "LogIn");
        
    }

        // POST: ApartmentResidents/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Resident_id,FirstName,LastName,FlatNumber,OwnerShipType,ContactNo,Email,MoveInDate")] ApartmentResident apartmentResident)
        {
            if (ModelState.IsValid)
            {
                db.ApartmentResidents.Add(apartmentResident);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(apartmentResident);
        }
        [ChildActionOnly]

        // GET: ApartmentResidents/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApartmentResident apartmentResident = db.ApartmentResidents.Find(id);
            if (apartmentResident == null)
            {
                return HttpNotFound();
            }
            return View(apartmentResident);
        }

        // POST: ApartmentResidents/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Resident_id,FirstName,LastName,FlatNumber,OwnerShipType,ContactNo,Email,MoveInDate")] ApartmentResident apartmentResident)
        {
            if (ModelState.IsValid)
            {
                db.Entry(apartmentResident).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(apartmentResident);
        }

        // GET: ApartmentResidents/Delete/5
        [ChildActionOnly]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApartmentResident apartmentResident = db.ApartmentResidents.Find(id);
            if (apartmentResident == null)
            {
                return HttpNotFound();
            }
            return View(apartmentResident);
        }

        // POST: ApartmentResidents/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ApartmentResident apartmentResident = db.ApartmentResidents.Find(id);
            db.ApartmentResidents.Remove(apartmentResident);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
