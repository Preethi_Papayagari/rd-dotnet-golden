﻿using Apartment.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Apartment.Controllers
{
    public class LogInController : Controller
    {
        // GET: LogIn

        [Route("LogInPage")]
        public ActionResult LogIn()
        {
            return View();
        }
        
        [HttpPost]
        public ActionResult LogIn(User USer)
        {
            using (Apartment_Expenses2Entities lb = new Apartment_Expenses2Entities())
            {
                var re = lb.Users.Where(x => x.UserName == USer.UserName && x.UserPassword == USer.UserPassword);
                if (re.Count() != 0)
                {
                    //Session["Name"] = user.Name;
                    FormsAuthentication.SetAuthCookie(USer.UserName, false);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    TempData["Error"] = "Incorrect UserName and Password";
                }
            }
            return View();
        }

        public ActionResult Logout() {
            FormsAuthentication.SignOut();
            return RedirectToAction("LogIn");
        }
    }
}