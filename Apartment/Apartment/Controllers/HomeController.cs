﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;

namespace Apartment.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {

            if (!User.Identity.Name.IsEmpty())
            {
                return View();
            }
            return RedirectToAction("LogIn", "LogIn");
        }

    }
}