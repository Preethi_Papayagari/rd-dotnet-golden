﻿using Apartment.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Apartment.Controllers
{
    public class MonthlyController : Controller
    {
        HttpClient httpClient;
        public MonthlyController()
        {
            httpClient=new HttpClient();
            Uri u = new Uri("http://localhost:54026/api/");
            httpClient.BaseAddress = u;
        }

        public ActionResult Index()
        {

            List<MonthlyMaintainance> li = new List<MonthlyMaintainance>();
            HttpResponseMessage reposnse = httpClient.GetAsync(httpClient.BaseAddress + "/Monthly").Result;
            if (reposnse.IsSuccessStatusCode)
            {
                string Data = reposnse.Content.ReadAsStringAsync().Result;
                li = JsonConvert.DeserializeObject<List<MonthlyMaintainance>>(Data);
            }
            return View(li);

        }
    }
}