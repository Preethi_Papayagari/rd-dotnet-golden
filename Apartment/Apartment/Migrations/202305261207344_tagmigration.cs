﻿namespace Apartment.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tagmigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        TagID = c.Int(nullable: false, identity: true),
                        TagName = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.TagID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Tags");
        }
    }
}
