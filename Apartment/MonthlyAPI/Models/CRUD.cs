﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MonthlyAPI.Models
{
    public class CRUD
    {
        SqlConnection con;
        string path = ConfigurationManager.ConnectionStrings["MyPath"].ConnectionString;
        public CRUD()
        {
            con = new SqlConnection(path);
        }

        public List<MonthlyMaintainance> GetList()
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("GetAll", con);
            SqlDataReader r = cmd.ExecuteReader();
            List<MonthlyMaintainance> list = new List<MonthlyMaintainance>();
           while(r.Read())
            {
                MonthlyMaintainance m=new MonthlyMaintainance() { ResidentId = (int)r[1], Month = (string)r[2], Year = (int)r[3], MaintenanceAmount = (decimal)r[4], Note = (string)r[5] };
                list.Add(m);
            }
            con.Close();
           return list;
        }
        public void Insert(MonthlyMaintainance m )
        {
            
            con.Open();
            SqlCommand cmd = new SqlCommand("InsertMonthly", con);
            cmd.CommandType=System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@RId", m.ResidentId);
            cmd.Parameters.AddWithValue("@Month",m.Month);
            cmd.Parameters.AddWithValue("@Year",m.Year);
            cmd.Parameters.AddWithValue("@Amount",m.MaintenanceAmount);
            cmd.Parameters.AddWithValue("@Note",m.Note);
            cmd.ExecuteNonQuery();
            con.Close();
        }
    }
}