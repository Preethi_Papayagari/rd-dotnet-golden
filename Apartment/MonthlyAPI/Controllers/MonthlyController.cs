﻿using MonthlyAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MonthlyAPI.Controllers
{
    public class MonthlyController : ApiController
    {
        CRUD c = new CRUD();

        [HttpGet]
        public List<MonthlyMaintainance> Get()
        {
            return c.GetList();
        }

        [HttpPost]
        public void Post(MonthlyMaintainance m)
        {
            c.Insert(m);
        }

    }
}
