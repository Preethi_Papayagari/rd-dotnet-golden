--creation of the database-----
create database Apartment_Expenses
 ---Using the database----
 use Apartment_Expenses


 ---Creating Table ApartmentResidents-----

 create table ApartmentResidents (Resident_id int primary key identity(1000,1),FirstName varchar(40),LastName varchar(40),FlatNumber int,OwnerShipType varchar(50),ContactNo int,Email varchar(50),MoveInDate Date)

 ----Creating MonthlyMaintainance-----
 create table MonthylyMaintainance (ID int primary key identity,ResidentId int references	ApartmentResidents(Resident_id),Month varchar(20),Year int ,MaintenanceAmount decimal(10,2),Note varchar(200))


 --Procedures for MonthlyMaintainance------

 --Insertion----
create procedure InsertMonthly
@RId int,
@Month varchar(20),
@Year varchar(20),
@Amount decimal(10,2),
@Note varchar(200)
as
begin
insert into MonthylyMaintainance (ResidentId,Month,Year,MaintenanceAmount,Note) values(@RId,@Month,@Year,@Amount,@Note)
end

---For Fetching-----
create procedure GetAll
as
begin
select * from MonthylyMaintainance
end

---Creating Table PaymentMethods-----

 create table PaymentMethods(PaymentId int primary key identity,PaymentMethodName varchar(50),Description varchar(100),IsActive int )


 ---Creating Table Users-----
 create table Users (UserID int primary key identity , UserName varchar(50),UserPassword varchar(100),Email varchar(100),FullName varchar(100),CreatedAt datetime,UpdateAt datetime)

 ---Procedure for Users--

 --Insertion---

 create procedure InsertUsers 
 @UserName varchar(50),
 @Password varchar(100),
 @Email varchar(100),
 @FullName varchar(100)
 as
 begin
 insert into Users  (UserName,UserPassword ,Email,FullName ,CreatedAt ,UpdateAt)  values(@UserName,@Password,@Email,@FullName,GETDATE(),GETDATE())
 end

 --Updation---

 create procedure UpdateUsers
  @UserName varchar(50),
 @Password varchar(100),
 @Email varchar(100),
 @FullName varchar(100)
 as
 begin
 update Users set UserName=@UserName,UserPassword=@Password,Email=@Email,FullName=@FullName,UpdateAt=GETDATE()
 end

---To Get User-------
alter procedure GetUser
 @UserId int
 as
 begin
 select UserName,Email,FullName,UserPassword from Users where UserId=@UserId
end

